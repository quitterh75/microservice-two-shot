function ShoeList({shoes, getShoes}) {

    async function handleShoeDelete(shoeid) {
        const shoeUrl = 'http://localhost:8080/api/shoes/${shoeid}';
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            console.log("deleted");
            getShoes();
        }
    }
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Brand</th>
                        <th>Color</th>
                        <th>Shoe Bin and Num</th>
                        <th>Shoe Pic</th>
                        <th>Garbage</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe=> {
                        return (
                            <tr>key={shoe.id}
                                <td>{shoe.model}</td>
                                <td>{shoe.brand}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.closet_name}-{shoe.bin.bin_number}</td>
                                <td> <img src={shoe.picture_url} alt="shoe" style={{ width: '100px', height: '100px'}} /></td>
                                <td>
                                    <button onClick={() => handleShoeDelete(shoe.id)} className="btn btn-delete"> Throw away this shoe?</button>
                                </td>
                            </tr>
                        );
                        })}

                </tbody>
            </table>
        );
    }
        export default ShoeList
    
        
