import React, { useState } from "react";

function ShoeForm({getShoe, bins}) {
    const [model, setModel] = useState('');
    const [brand, setBrand] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');


    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            model: model,
            brand: brand,
            picture_url: picture_url,
            color: color,
            bin: bin,
        };
    


    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
        const newBin = await response.json();
        console.log(newBin);
        setModel('');
        setBrand('');
        setPictureUrl('');
        setColor('');
        setBin('');
        getShoe();
    }
}

    function handleBrandChange(event) {
        const { value } = event.target;
        setBrand(value);
    }

    function handleModelChange(event) {
        const { value } = event.target;
        setModel(value);

    }
    function handlePictureUrlChange(event) {
        const { value } = event.target;
        setPictureUrl(value);
    }
    function handleColorChange(event) {
        const { value } = event.target;
        setColor(value);
    }
    function handleBinChange(event) {
        const { value } = event.target;
        setBin(value);
    }
    

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Shoe</h1>
              <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                  <input onChange={handleModelChange} value={model} maxLength={100} placeholder="Model" required type="text" id="model" className="form-master" />
                  <label htmlFor="model">Model</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleBrandChange} value={brand} maxLength={100} placeholder="Brand" type="text" id="brand" className="form-master" required/>
                  <label htmlFor="brand">Brand</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePictureUrlChange} value={picture_url} placeholder="PictureUrl" type="url" id="picture_url" className="form-master" />
                  <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColorChange} value={color} maxLength={100} id="color" className="form-master"></input>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleBinChange} value={bin.href} required className="form-select" id="bin">
                    <option value="">Choose a bin</option>
                    {bins.map(bin => {
                      return (
                        <option key={bin.id} value={bin.href}>{bin.closet_name} - {bin.bin_number}</option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
                }
    
    export default ShoeForm;
    