import React, { useState, useEffect } from 'react';

function HatForm() {
  const [locations, setLocations] = useState([])


  const [formData, setFormData] = useState({
    fabric: '',
    style_name: '',
    color_name: '',
    picture: '',
    location: '',
  })


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8090/api/hats/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
          'Content-Type': 'application/json',
      },
    };


    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setFormData({
        fabric: '',
        style_name: '',
        color_name: '',
        picture: '',
        location: '',
    })
    }
    window.location.reload()
  }

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value
      // ...(inputName === "location" && {[inputName]: Number(value)}),
      // ...(inputName !== "location" && {[inputName]: value}),
    });
  }
  
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input placeholder="Fabric" onChange={handleFormChange} required type="text" name="fabric" id="fabric" className="form-control"/>
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Style" onChange={handleFormChange} required type="text" name="style_name" id="style_name" className="form-control"/>
              <label htmlFor="style_name">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Color" onChange={handleFormChange} required type="text" name="color_name" id="color_name" className="form-control"/>
              <label htmlFor="color_name">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="Picture" onChange={handleFormChange} required url="text" name="picture" id="picture" className="form-control"/>
              <label htmlFor="picture">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange}  required name="location" id="location" className="form-select">
              <option value=''>Choose a Location</option>
              {locations.map(location => {
                return(
                  <option key={location.id} value={location.id}>{location.closet_name} - {location.section_number}/{location.shelf_number}</option>
                )
                })
              }</select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );   
}
export default HatForm;
