function HatList({ hats, getHats }) {
  
  const handleDelete = async (event, id) => {
    event.preventDefault();
    const url = `http://localhost:8090/api/hats/${id}`;

    const fetchConfig = {
      method: "DELETE",
      headers: {
          'Content-Type': 'application/json',
      }
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getHats();
    }
}

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Picture</th>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Location</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td><img src={hat.picture} style={{width: "100px", height: "100px"}} alt="Image"/></td>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color_name }</td>
              <td>{ hat.location.closet_name }</td>
              <td className="flex-container center-center" style={{width: "auto", height: "auto"}}>
                <button onClick={(event) => handleDelete(event, hat.id)} className="btn btn-link" style={{width: "auto", height: "100px"}}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    );
  }

export default HatList;
