# Wardrobify

Team:

- Nadine C. - Hats Microservice
- Hudson Q. - Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
I need to be able to list the shoes. Update shoes and delete them. Create models for shoes

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I'll need a model for hats
Need to be able to show hat, create hat, delete hat
