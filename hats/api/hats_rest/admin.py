from django.contrib import admin
from .models import Hat

# Register your models here.


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    "id",
    "fabric",
    "style_name",
    "color_name",
    "picture",
