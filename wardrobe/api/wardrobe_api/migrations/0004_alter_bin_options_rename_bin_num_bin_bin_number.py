# Generated by Django 4.0.3 on 2023-07-23 23:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wardrobe_api', '0003_alter_bin_options_rename_bin_number_bin_bin_num'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bin',
            options={'ordering': ('closet_name', 'bin_number', 'bin_size')},
        ),
        migrations.RenameField(
            model_name='bin',
            old_name='bin_num',
            new_name='bin_number',
        ),
    ]
