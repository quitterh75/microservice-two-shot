from django.db import models
from django.urls import reverse


class BinVO(models.Model):

    import_href = models.CharField(max_length=200, unique=True),
    closet_name = models.CharField(max_length=200),
    bin_num = models.PositiveBigIntegerField(),


class Shoe(models.Model):
    model = models.CharField(max_length=100)
    brand = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()

    bin = models.ForeignKey(
        BinVO, related_name="shoes", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.model} - {self.brand} - {self.color}"

    def get_apir_url(self):
        return reverse("api_show_bin", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("model", "brand", "color")
